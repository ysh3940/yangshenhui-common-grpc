/**
 * Package name:indi.yangshenhui.common.service
 * File name:HelloWorldImpl.java
 * Date:2016年8月31日-下午2:05:31
 * feiniu.com Inc.Copyright (c) 2013-2015 All Rights Reserved.
 *
 */
package indi.yangshenhui.common.grpc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import indi.yangshenhui.common.grpc.HelloReply;
import indi.yangshenhui.common.grpc.HelloRequest;
import indi.yangshenhui.common.grpc.HelloWorldGrpc;
import io.grpc.stub.StreamObserver;

/**
 * @ClassName HelloWorldImpl
 * @Description 实现定义的接口方法
 * @date 2016年8月31日 下午2:05:31
 * @author shenhui.yang
 * @version 1.0.0
 *
 */
public class HelloWorldImpl extends HelloWorldGrpc.HelloWorldImplBase {
	private static final Logger log = LoggerFactory.getLogger(HelloWorldImpl.class);

	@Override
	public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
		log.info("request param: {}", request.getName());
		HelloReply reply = HelloReply.newBuilder().setMessage("sayHello : Hello " + request.getName() + "!").build();
		responseObserver.onNext(reply);
		responseObserver.onCompleted();
	}

	@Override
	public void sayHello2(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
		String name = request.getName();
		log.info("request param: {}", name);

		for (int i = 1; i <= 10; i++) {
			HelloReply reply = HelloReply.newBuilder().setMessage("sayHello2 : Hello " + request.getName() + i + "!")
					.build();
			responseObserver.onNext(reply);
		}
		responseObserver.onCompleted();
	}

	@Override
	public StreamObserver<HelloRequest> sayHello3(final StreamObserver<HelloReply> responseObserver) {
		return new StreamObserver<HelloRequest>() {
			private StringBuffer name;

			@Override
			public void onCompleted() {
				HelloReply reply = HelloReply.newBuilder().setMessage("sayHello3 : Hello " + name.toString() + "!")
						.build();
				responseObserver.onNext(reply);
				responseObserver.onCompleted();
			}

			@Override
			public void onError(Throwable arg0) {
				log.error(arg0.getMessage(), arg0);
			}

			@Override
			public void onNext(HelloRequest arg0) {
				if (name == null) {
					name = new StringBuffer();
				}
				name.append(" ").append(arg0.getName());
				log.info("request param: {}", name.toString());
			}

		};
	}

	@Override
	public StreamObserver<HelloRequest> sayHello4(final StreamObserver<HelloReply> responseObserver) {
		return new StreamObserver<HelloRequest>() {

			@Override
			public void onCompleted() {
				responseObserver.onCompleted();
			}

			@Override
			public void onError(Throwable arg0) {
				log.error(arg0.getMessage(), arg0);
			}

			@Override
			public void onNext(HelloRequest arg0) {
				log.info("request param: {}", arg0.getName() + " " + System.currentTimeMillis());
				HelloReply reply = HelloReply.newBuilder()
						.setMessage("sayHello3 : Hello " + (arg0.getName() + " " + System.currentTimeMillis()) + "!")
						.build();
				responseObserver.onNext(reply);
			}

		};
	}

}
